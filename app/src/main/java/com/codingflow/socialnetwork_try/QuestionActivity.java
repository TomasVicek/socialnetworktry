package com.codingflow.socialnetwork_try;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class QuestionActivity extends AppCompatActivity {
    DatabaseReference dbPost;
    TextView QuestionText, description;
    RadioButton Option1RadioButton, Option2RadioButton,Option3RadioButton;
    ImageView QuestionImage;
    RadioButton radioButton;
    DatabaseReference AnswersRef;
    private FirebaseAuth mAuth;
    String current_user_id;
    String questionID,option1,option2,option3;
    private String saveCurrentDate, saveCurrentTime, AnswerTime;
    String OptionChosen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        QuestionText = findViewById(R.id.question_text_qactivity);
        Option1RadioButton = findViewById(R.id.option_qactivity);
        Option2RadioButton = findViewById(R.id.option2_qactivity);
        Option3RadioButton = findViewById(R.id.option3_qactivity);
        QuestionImage = findViewById(R.id.image_qactivity);
        description = findViewById(R.id.question_description_qactivity);
        Log.d("QuestionActivity", "Questiontext");


        mAuth = FirebaseAuth.getInstance();
        current_user_id = mAuth.getCurrentUser().getUid();


        final String ID = getIntent().getStringExtra("ID");

        dbPost = FirebaseDatabase.getInstance().getReference().child("Posts").child(ID);
        dbPost.addValueEventListener(postListener);

        addListenerOnButton();

        AnswersRef = FirebaseDatabase.getInstance().getReference().child("Answers");

    }


    ValueEventListener postListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // Get Post object and use the values to update the UI
            PostObject post = dataSnapshot.getValue(PostObject.class);
            QuestionText.setText(post.Question);
            description.setText(post.description);
            option1=post.Option1;
            option2=post.Option2;
            option3=post.Option3;
            Option1RadioButton.setText(post.Option1);
            Option2RadioButton.setText(post.Option2);
            Option3RadioButton.setText(post.Option3);
            Picasso.get().load(post.postimage).into(QuestionImage);
            questionID = post.ID;
            //Toast.makeText(QuestionActivity.this,questionID,Toast.LENGTH_SHORT).show();


            // ...
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Getting Post failed, log a message

            // ...
        }
    };

    public void addListenerOnButton() {

        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_group_qactivity);
        Button btnDisplay = (Button) findViewById(R.id.confirm_answer_activity);

        btnDisplay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);
                OptionsSwitch(selectedId);
                SavingPostInformationToDatabase();


            }

        });

    }

    private void Time() {
        Calendar calFordDate = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd-MMMM-yyyy");
        saveCurrentDate = currentDate.format(calFordDate.getTime());

        Calendar calFordTime = Calendar.getInstance();
        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss");
        saveCurrentTime = currentTime.format(calFordDate.getTime());

        AnswerTime = saveCurrentDate + saveCurrentTime;


    }

    private void SavingPostInformationToDatabase() {
        Time();


        AnswersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String AnswerID = current_user_id + AnswerTime;
                    Toast.makeText(QuestionActivity.this, AnswerID, Toast.LENGTH_SHORT).show();


                    HashMap postsMap = new HashMap();
                    postsMap.put("uid", current_user_id);
                    postsMap.put("date", saveCurrentDate);
                    postsMap.put("time", saveCurrentTime);
                    postsMap.put("QuestionID", questionID);
                    postsMap.put("optionchosen", OptionChosen);

                    postsMap.put("ID", AnswerID);

                    AnswersRef.child(AnswerID).updateChildren(postsMap)
                            .addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(QuestionActivity.this, "hello world.", Toast.LENGTH_SHORT).show();
                                        SendUserToQueryActivity();

                                    } else {
                                        Toast.makeText(QuestionActivity.this, "Error Occured while updating your post.", Toast.LENGTH_SHORT).show();

                                    }

                                }
                            });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void OptionsSwitch(int selectedID) {
        switch (selectedID) {

            case (R.id.option_qactivity):
                OptionChosen = "1";
                break;

            case (R.id.option2_qactivity):
                OptionChosen = "2";
                break;

            case (R.id.option3_qactivity):
                OptionChosen = "3";
                break;


        }
    }
    private void SendUserToQueryActivity() {
        Intent queryIntent = new Intent(QuestionActivity.this, QueryAcivity.class);
        queryIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        queryIntent.putExtra("QuestionID",questionID);
        queryIntent.putExtra("Option1",option1);
        queryIntent.putExtra("Option2",option2);
        queryIntent.putExtra("Option3",option3);
        startActivity(queryIntent);

        finish();
    }
}



