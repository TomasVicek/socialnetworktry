package com.codingflow.socialnetwork_try;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

//Home button doesnt work
//Drawer layout is applied two times
public class MainActivity extends AppCompatActivity {

    private NavigationView navigationView;
    private DrawerLayout drawer_layout;

    private ActionBarDrawerToggle ActionBarDrawerToggle;
    private android.support.v7.widget.Toolbar mToolbar;
    private Button login_button;
    private CircleImageView NavProfileImage;
    private TextView NavProfileUserName;

    private FirebaseAuth mAuth;
    private DatabaseReference UsersRef;
    String currentUserID;
    String fullname;
    BottomNavigationView bottomNavView;
    private RecyclerView recyclerView;
    private PostAdapter adapter;
    private List<PostObject> postList;

    DatabaseReference dbPost;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        //if (mAuth.getUid()!=null)
        currentUserID = mAuth.getCurrentUser().getUid();
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");


        navigationView=findViewById(R.id.navigation_view);
        Toolbar mToolbar=findViewById(R.id.main_page_toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        View navView = navigationView.inflateHeaderView(R.layout.navigation_header);
        NavProfileUserName = navView.findViewById(R.id.nav_user_full_name);
        NavProfileImage=navView.findViewById(R.id.nav_profile_image);
        bottomNavView = findViewById(R.id.bottom_navigation_view);


//Action bar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Home");
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
               this, drawer, mToolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

//recycler
        recyclerView = findViewById(R.id.posts_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        postList = new ArrayList<>();
        adapter = new PostAdapter(this, postList);
        recyclerView.setAdapter(adapter);

        //1. SELECT * FROM Artists
        dbPost = FirebaseDatabase.getInstance().getReference("Posts");
        dbPost.addListenerForSingleValueEvent(valueEventListener);



        UsersRef.child(currentUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.exists())
                {
                    if(dataSnapshot.hasChild("fullname"))
                    {
                        String fullname = dataSnapshot.child("fullname").getValue().toString();

                        NavProfileUserName.setText(fullname);


                    }
                    if(dataSnapshot.hasChild("profileimage"))
                    {
                        String image = dataSnapshot.child("profileimage").getValue().toString();
                        //NavProfileUserName.setText(image);

                        Picasso.get().load(image).placeholder(R.drawable.profile).into(NavProfileImage);
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this, "Profile name do not exists...", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {


                UserMenuDrawerSelector(item);
                return false;
            }

        }

        );
       bottomNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                UserMenuBottomSelector(menuItem);
                return false;
            }
        });


    }


    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            postList.clear();
            if (dataSnapshot.exists()) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    PostObject post = snapshot.getValue(PostObject.class);
                    postList.add(post);


                }
                adapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public boolean onOptionsItemSelected(MenuItem item){

        if(ActionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onStart(){
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null){
        SendUserToLoginActivity();
        }
        else{
            CheckUserExistence();
        }
    }

    private void UserMenuDrawerSelector(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.nav_profile:
                Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
                SendUserToSetupActivity();
                break;

            case R.id.nav_home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                break;


            case R.id.nav_logout:
                mAuth.signOut();
                SendUserToLoginActivity();
                break;
        }
        }

    private void UserMenuBottomSelector(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.bottom_navigation_home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                SendUserToMainActivity();

                break;

            case R.id.bottom_navigation_addpost:
                Toast.makeText(this, "add Post", Toast.LENGTH_SHORT).show();
                SendUserToAddPostActivity();
                break;


            case R.id.bottom_navigation_notifications:
                Toast.makeText(this, "notifications", Toast.LENGTH_SHORT).show();
                break;

            case R.id.bottom_navigation_search:
                Toast.makeText(this, "search", Toast.LENGTH_SHORT).show();
                break;
        }

    }


    private void SendUserToLoginActivity()
    {   Intent loginIntent = new Intent(MainActivity.this,LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();

    }

    private void CheckUserExistence()
    {
        final String current_user_id = mAuth.getCurrentUser().getUid();

        UsersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(!dataSnapshot.hasChild(current_user_id))//there should be !
                    {
                   SendUserToSetupActivity();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    private void SendUserToSetupActivity()
    {
        Intent setupIntent = new Intent(MainActivity.this, SetupActivity.class);
        setupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(setupIntent);
        finish();
    }

    private void SendUserToAddPostActivity()
    {Intent setupIntent = new Intent(MainActivity.this, AddPostActivity.class);
        setupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(setupIntent);
        finish();

    }

    private void SendUserToMainActivity(){
        Intent mainIntent = new Intent(MainActivity.this,MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);

    }





}
