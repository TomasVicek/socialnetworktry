package com.codingflow.socialnetwork_try;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Pie;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class QueryAcivity extends AppCompatActivity {
    String optionchosen;
    int option1count,option2count,option3count;
    String option1,option2,option3;
    private TextView option1countTextView, option2countTextView, option3countTextView,option1TextView,option2TextView,option3TextView;
    DatabaseReference dbAnswers;
    Intent questionIntent;
    String questionID;
    BottomNavigationView bottomNavView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph);
       /* option1countTextView =findViewById(R.id.option1_count);
        option2countTextView =findViewById(R.id.option2_count);
        option3countTextView =findViewById(R.id.option3_count);
        option1TextView=findViewById(R.id.option1_text);
        option2TextView=findViewById(R.id.option2_text);
        option3TextView=findViewById(R.id.option3_text);
        */bottomNavView=findViewById(R.id.bottom_navigation_view);


        questionIntent =getIntent();
        questionID=questionIntent.getStringExtra("QuestionID");
        option1=questionIntent.getStringExtra("Option1");
        option2=questionIntent.getStringExtra("Option2");
        option3=questionIntent.getStringExtra("Option3");

/*
        option1TextView.setText(option1);
        option2TextView.setText(option2);
        option3TextView.setText(option3);
*/

        Query query = FirebaseDatabase.getInstance().getReference("Answers")
                .orderByChild("QuestionID")
                .equalTo(questionID);
        query.addListenerForSingleValueEvent(valueEventListener);
        option3count=0;
        option2count=0;
        option1count=0;

        bottomNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                UserMenuBottomSelector(menuItem);
                return false;
            }
        });



    }

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    optionchosen = snapshot.child("optionchosen").getValue().toString();
                    optionsCount();


                }
                Pie pie = AnyChart.pie();

                List<DataEntry> data = new ArrayList<>();
                data.add(new ValueDataEntry(option1, option1count));
                data.add(new ValueDataEntry(option2, option2count));
                data.add(new ValueDataEntry(option3, option3count));

                pie.data(data);

                AnyChartView anyChartView = (AnyChartView) findViewById(R.id.any_chart_view);
                anyChartView.setChart(pie);





               /* option1countTextView.setText(option1count+"");
                option2countTextView.setText(option2count+"");
                option3countTextView.setText(option3count+"");*/

            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private void optionsCount(){
        switch (optionchosen){

            case("1"):option1count++;
            break;
            case ("2"):option2count++;
            break;
            case("3"):option3count++;
            break;
        }
    }

    private void UserMenuBottomSelector(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.bottom_navigation_home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                SendUserToMainActivity();

                break;

            case R.id.bottom_navigation_addpost:
                Toast.makeText(this, "add Post", Toast.LENGTH_SHORT).show();
                SendUserToAddPostActivity();
                break;


            case R.id.bottom_navigation_notifications:
                Toast.makeText(this, "notifications", Toast.LENGTH_SHORT).show();
                break;

            case R.id.bottom_navigation_search:
                Toast.makeText(this, "search", Toast.LENGTH_SHORT).show();
                break;
        }

    }
    private void SendUserToAddPostActivity()
    {Intent setupIntent = new Intent(QueryAcivity.this, AddPostActivity.class);
        startActivity(setupIntent);
        finish();

    }
    private void SendUserToMainActivity(){
        Intent mainIntent = new Intent(QueryAcivity.this,MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);

        }

}
