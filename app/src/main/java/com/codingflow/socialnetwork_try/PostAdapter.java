package com.codingflow.socialnetwork_try;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

    /**
     * Created by Belal on 4/17/2018.
     */

    public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostObjectViewHolder> {

        private Context mCtx;
        private List<PostObject> PostObjectList;

        public PostAdapter(Context mCtx, List<PostObject> PostObjectList) {
            this.mCtx = mCtx;
            this.PostObjectList = PostObjectList;
        }

        @NonNull
        @Override
        public PostObjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mCtx).inflate(R.layout.post, parent, false);
            return new PostObjectViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull PostObjectViewHolder holder, final int position) {
            final PostObject PostObject = PostObjectList.get(position);
            holder.textViewQuestion.setText(PostObject.Question);
            Picasso.get().load(PostObject.postimage).into(holder.imageViewImage);


            holder.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {




                    Intent intent = new Intent(mCtx, QuestionActivity.class);
                    intent.putExtra("ID", PostObject.ID);
                    mCtx.startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return PostObjectList.size();
        }

        class PostObjectViewHolder extends RecyclerView.ViewHolder {

            TextView textViewQuestion;
            ImageView imageViewImage;
            LinearLayout parentLayout;

            public PostObjectViewHolder(@NonNull View itemView) {
                super(itemView);

                textViewQuestion = itemView.findViewById(R.id.question_text);
                imageViewImage = itemView.findViewById(R.id.post_profile_image);
                parentLayout = itemView.findViewById(R.id.post_parent_layout);




            }
        }
    }


